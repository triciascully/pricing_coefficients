"""
Author:
Tricia Scully
Pricing Strategy & Analytics Manager
3.23.2020

- Code Details - 

Business Purpose: Convert Josiah's SAS code to python (SAS code produces custom pricing coefficients)
Data sources: AIP - revenue_management, josiah_johnson, and core databases
Region: N/A
Brand: N/A
Segment: N/A
Pack Size: N/A
SKU: N/A
Timeframe: N/A


Notes: 
"""

#Import all required packages
#import numpy as np
import pandas as pd
#from scipy import stats
import pyodbc

def impala_select(cnxn,query,show=True,sort_col=None):
    data = pd.read_sql(query,cnxn)
    if show:
        if sort_col:
            print(data.sort_values(sort_col))
        else:
            print(data)
    return data


# connect to Impala
cnxn = pyodbc.connect("DSN=Impala", autocommit=True)
cursor = cnxn.cursor()

#se up queries to pull data from Impala into dataframes
## Store level data from Kroger
stores_query = "select * from core.acn_outlet_dim where state_abbreviation in ('ca') "
kr_norcal_g_query = "select * from josiah_johnson.kroger_cali where planningunitnme <> 'pu - southern ca'"
aod_store_level_query_p1 = "select * from josiah_johnson.aod_store_level_norcal_p1 where units > 0"
aod_store_level_query_p2 = "select * from josiah_johnson.aod_store_level_norcal_p2 where units > 0"
aod_store_level_query_p3 = "select * from josiah_johnson.aod_store_level_norcal_p3 where units > 0"
aod_store_level_query_p4 = "select * from josiah_johnson.aod_store_level_norcal_p4 where units > 0"

#strs_query = "select distinct tdoutletcd from josiah_johnson.aod_store_level_norcal_p1"

#pull in data from Impala, create dfs
stores = impala_select(cnxn,stores_query)

kr_norcal_g = impala_select(cnxn,kr_norcal_g_query)
kr_norcal_g.drop(['rpt_short_desc', 're_sto_num'], axis=1)
kr_norcal_g = kr_norcal_g.rename(columns={'store_state': 'state_abbreviation', 'calendarfiscalwe': 'weekendingdate', 'scanned_retail_dollars': 'dollar',
                                          'store_banner': 'store_name', 'scanned_movement': 'units'})
    
aod_store_level_norcal_p1 = impala_select(cnxn,aod_store_level_query_p1)
aod_store_level_norcal_p2 = impala_select(cnxn,aod_store_level_query_p2)
aod_store_level_norcal_p3 = impala_select(cnxn,aod_store_level_query_p3)
aod_store_level_norcal_p4 = impala_select(cnxn,aod_store_level_query_p4)

aod_store_level_work = aod_store_level_norcal_p1['weekendingdate'].unique()

strs = aod_store_level_norcal_p1['tdoutletcd'].unique()

#sample .6 of stores
strs = pd.DataFrame(data=strs)

samplesrs = strs.sample(frac = 0.6)
samplesrs.sort_values(by=[0], inplace=True)
samplesrs.drop_duplicates(keep=False,inplace=True) 

#define brand/ounce/pack variables
aod_store_lvl_norcal_g = aod_store_level_norcal_p1.copy()
aod_store_lvl_norcal_g['ounces'] = aod_store_lvl_norcal_g['mc_ounce_c'].str.replace('oz', '')
indexNames = aod_store_lvl_norcal_g[ (aod_store_lvl_norcal_g['ounces'] == None) | (aod_store_lvl_norcal_g['ounces'] == 'rem ounce') | (aod_store_lvl_norcal_g['ounces'].isnull()) ].index
aod_store_lvl_norcal_g.drop(indexNames , inplace=True)
aod_store_lvl_norcal_g['ounces'] = aod_store_lvl_norcal_g['ounces'].astype(float)
aod_store_lvl_norcal_g['oz'] = round(aod_store_lvl_norcal_g['ounces'])
aod_store_lvl_norcal_g['oz'] = aod_store_lvl_norcal_g['oz'].astype(int)
aod_store_lvl_norcal_g = aod_store_lvl_norcal_g[aod_store_lvl_norcal_g['oz'].notna()]

aod_store_lvl_norcal_g['pack_size'] = aod_store_lvl_norcal_g['mc_package_size_c'].str.replace('pk', '')
indexNames = aod_store_lvl_norcal_g[ (aod_store_lvl_norcal_g['pack_size'] == 'keg') | (aod_store_lvl_norcal_g['pack_size'] == None) | (aod_store_lvl_norcal_g['pack_size'] == 'rem pack size') ].index
aod_store_lvl_norcal_g.drop(indexNames , inplace=True)

aod_store_lvl_norcal_g['brfam'] = aod_store_lvl_norcal_g['mc_brand_family_c'].str.replace(' ','')
##take out special characters
aod_store_lvl_norcal_g['brfam'] = aod_store_lvl_norcal_g['brfam'].str.lower()

aod_store_lvl_norcal_g['cseq_vol'] = (aod_store_lvl_norcal_g['units'].astype(int)*aod_store_lvl_norcal_g['pack_size'].astype(int)*aod_store_lvl_norcal_g['ounces'].astype(float))/228
indexNames = aod_store_lvl_norcal_g[ (aod_store_lvl_norcal_g['cseq_vol'] <0) ].index
aod_store_lvl_norcal_g.drop(indexNames , inplace=True)

aod_store_lvl_norcal_g['brpx'] = aod_store_lvl_norcal_g['brfam'].astype(str) + '_' + aod_store_lvl_norcal_g['oz'].astype(str) +'_'+ aod_store_lvl_norcal_g['pack_size'].astype(str)

aod_store_lvl_norcal_g.sort_values(by=['weekendingdate', 'tdoutletcd', 'upc'], inplace=True)
aod_store_lvl_norcal_g.drop_duplicates(keep=False,inplace=True) 

brpx_all = aod_store_lvl_norcal_g['brpx'].unique()

strs_kr = kr_norcal_g['tdoutletcd'].unique()

#Pull store sample from kroger db
SampleSRS_kr = strs_kr.sample(frac = 0.6)
SampleSRS_kr.sort_values(by=[0], inplace=True)
SampleSRS_kr.drop_duplicates(keep=False,inplace=True) 

kr__norcal_g_samp = kr_norcal_g[kr_norcal_g['tdoutletcd'].isin(SampleSRS_kr)]

# macro sample set
aod_store_level = aod_store_level_norcal_p1.sort_values(by=['weekendingdate','tdoutletcd','upc'],inplace = True).drop_duplicates(keep=False,inplace=True)
aod_store_level = pd.merge(aod_store_level, samplesrs, on='tdoutletcd') #default join in python is inner join

#combine kroger data with first set of grocery data on initial macro iteration

allgroc_norcal_g = pd.concat([aod_store_level, kr__norcal_g_samp])

#define new custom product groups

aod_store_lvl_norcal_g_1 = allgroc_norcal_g.copy()
aod_store_lvl_norcal_g_1.replace(to_replace='abita_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='alaskan_12_6', value='craft_12_6_899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='aleindustries_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='alesmith_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='alpine_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='anchor_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='andersonvalley_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='angelcity_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='auburnalehouse_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='avery_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='ballastpoint_12_6', value= 'craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='barrel_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='barrelhouse_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='bearrepublic_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='belchingbeaver_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='bigskybrewing_12_6', value='craft_12_6_799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='bluemoon_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='boulder_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='boulevard_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='breckenridge_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='brooklyn_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='buffalobills_12_6', value='craft_12_6_699', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='caldera_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='calicraft_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='cigarcity_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='coronado_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='deathvalley_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='deschutes_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='device_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='devilscanyon_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='dogfishhead_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='drakes_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='dustbowl_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='eelriver_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='elysian_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='epic_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='figueroamountain_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='firestone_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='flyingdog_12_6', value='craft_12_6_1599', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='fortpoint_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='founders_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='fourpeaks_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='fremont_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='goldenroad_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='gooseisland_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='gordonbiersch_12_6', value='craft_12_6_899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='greatbasin_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='greenflash_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='hangar_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='heretic_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='hopvalley_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='housebeer_12_6', value='craft_12_6_799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='jamaicabrand_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='karlstrauss_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='kernriver_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='killians_12_6', value='craft_12_6_799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='kneedeep_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='kombrewcha_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='kona_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='lagunitas_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='latitude_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='leftcoast_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='lefthand_12_6', value='craft_12_6_699', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='leinenkugels_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='lobotomy_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='lostcoast_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='luckylager_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='madriver_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='magnolia_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='mammoth_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='marin_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='maui_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='melvin_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='mikehess_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='mission_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='mojave_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='motherearthbrewco_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='newbelgium_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='newholland_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='ninkasi_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='northcoast_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='omission_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='oskarblues_12_6', value='craft_12_6_899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='outofbounds_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='owlsbrewradler_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='pyramid_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='redhook_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='revision_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='rogue_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='saintarcher_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='samueladams_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='santaclaravalley_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='seismic_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='shiner_12_6', value='craft_12_6_899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='shipyard_12_6', value='craft_12_6_899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='shocktop_12_6', value='craft_12_6_799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='sierranevada_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='sixrivers_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='ska_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='slobrew_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='snowshoe_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='soma_12_6', value='craft_12_6_899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='speakeasy_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='stamendment_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='steelhead_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='stone_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='strand_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='strike_12_6', value='craft_12_6_799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='sudwerk_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='sufferfest_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='tapit_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='thorn_12_6', value='craft_12_6_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='threeweavrs_12_6', value='craft_12_6_799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='tiogasequoia_12_6', value='craft_12_6_899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='townepark_12_6', value='craft_12_6_699', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='trackseven_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='traveler_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='trumer_12_6', value='craft_12_6_899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='turaalcoholickombucha_12_6', value='craft_12_6_1299', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='twpitchers_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='twpitchersradlers_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='twpitcherssnakebite_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='uinta_12_6', value='craft_12_6_1099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='widmerbrothers_12_6', value='craft_12_6_999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='wildpeak_12_6', value='craft_12_6_799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='alaskan_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='anchor_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='andersonvalley_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='ballastpoint_12_12', value='craft_12_12_2399', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='bearrepublic_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='bluemoon_12_12', value='bluemoon_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='brooklyn_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='coronado_12_12', value='craft_12_12_1899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='deschutes_12_12', value='craft_12_12_1899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='dogfishhead_12_12', value='craft_12_12_1899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='drakes_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='elysian_12_12', value='craft_12_12_2199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='figueroamountain_12_12', value='craft_12_12_1999', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='firestone_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='fortpoint_12_12', value='craft_12_12_1899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='founders_12_12', value='craft_12_12_1199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='gooseisland_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='gordonbiersch_12_12', value='craft_12_12_1599', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='hopvalley_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='housebeer_12_12', value='craft_12_12_1399', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='kona_12_12', value='craft_12_12_1599', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='lagunitas_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='lefthand_12_12', value='craft_12_12_1699', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='leinenkugels_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='lostcoast_12_12', value='craft_12_12_1699', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='madriver_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='magichat_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='marathon_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='maui_12_12', value='craft_12_12_2199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='motherearthbrewco_12_12', value='craft_12_12_2099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='newbelgium_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='northcoast_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='omission_12_12', value='craft_12_12_1699', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='oskarblues_12_12', value='craft_12_12_1899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='pyramid_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='redhook_12_12', value='craft_12_12_1599', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='saintarcher_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='samueladams_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='shocktop_12_12', value='craft_12_12_1399', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='sierranevada_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='stamendment_12_12', value='craft_12_12_1899', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='steelhead_12_12', value='craft_12_12_1599', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='stone_12_12', value='craft_12_12_2199', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='tiogasequoia_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='trackseven_12_12', value='craft_12_12_2099', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='trumer_12_12', value='craft_12_12_1799', inplace=True)
aod_store_lvl_norcal_g_1.replace(to_replace='widmerbrothers_12_12', value='craft_12_12_1799', inplace=True)

mask = (aod_store_lvl_norcal_g_1['pack_size'] == 15) & (aod_store_lvl_norcal_g_1['ounces'] > 12)
aod_store_lvl_norcal_g_1['brpx'][mask] = 'craft_12_12_1799'

top_brands_norcal_g_1 = aod_store_lvl_norcal_g_1[aod_store_lvl_norcal_g_1['brpx'].isin(['craft_12_12_1799','bluemoon_12_12_1799','craft_12_12_2399','craft_12_12_1899','craft_12_12_2199',
'craft_12_12_1999','craft_12_12_1199','craft_12_12_1599','craft_12_12_1399','craft_12_12_1699',
'craft_12_12_2099','craft_12_6_999','craft_12_6_899','craft_12_6_1099','craft_12_6_1199','craft_12_6_1299',
'craft_12_6_799','craft_12_6_699','craft_12_6_1599','craft_12_15'])]

pack = top_brands_norcal_g_1['brpx'].unique()

#create metric variables for each custom product group





























"""
### Functions included to handle Kerberos ticket/Hadoop access when running in Jupyter Notebooks on AIP (user must have keytab file created). ###

def impala_select(cnxn,query,show=True,sort_col=None):
    data = pd.read_sql(query,cnxn)
    if show:
        if sort_col:
            print(data.sort_values(sort_col))
        else:
            print(data)
    return data

def authenticate_kerberos(force=False):
    # get username
    username = get_user()
    # get kerberos realm
    krb_realm = "MILLER.LOCAL"
    
    # create 'klist" command
    cmd = "klist"
    # create linux command process
    process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
    # wait until done, get result
    result = process.communicate(None)[0].decode("utf-8") 
    
    # check that we don't already have a valid ticket
    found_username = False
    expire_date = None
    for line in result.split("\n"):
        if username in line:
            found_username = True
            # update realm if different
            krb_realm = line.split("@")[-1]
        line = line.split()
        if len(line) == 5:
            try:
                expire_date = datetime.strptime(line[2] + " " + line[3],'%m/%d/%Y %H:%M:%S')
            except (ValueError) as e:
                pass
    
    if (found_username) and (expire_date != None) and (expire_date > datetime.today()) and (not force):
        return True
    else:
        # create 'kdestory" command
        cmd = "kdestroy"
        # create linux command process
        process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
        # wait until done, get result
        result = process.communicate(None)[0].decode("utf-8") 

        # create "kinit" command
        cmd = "kinit " + username + "@" + krb_realm
        # create linux command process
        process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
        # wait until done, get result
        result = process.communicate(get_password("Password for " + username + "@" + krb_realm + ": ").encode('utf-8'))[0].decode("utf-8").strip()
        # if result
        if result.strip() != "Password for " + username + "@" + krb_realm + ":":
            print("Failed to obtained Kerberos Ticket.")
            # result
            print(result)
            return False
        # ticket got successfully
        else:
            print("Successfully obtained Kerberos Ticket.\n")
            return True

### Run Kerberos access functions, then run Hadoop query to get data. ###
success = authenticate_kerberos(force=False)
"""
